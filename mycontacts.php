<?php
/*
Plugin Name: My Contacts
Plugin URI: https://bolbolngaputi
Description: Add Contacts
Author: vergel
Version: 0.1
Author URI: http://wala
*/


function my_contacts_install() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	$contact_field = $wpdb->prefix."my_contacts_fields";
	$cf_sql = "CREATE TABLE IF NOT EXISTS $contact_field (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  field_name varchar(255) DEFAULT '' NOT NULL,
	  field_active int(9) DEFAULT 0 NOT NULL,
	  PRIMARY KEY  (id)
	) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $cf_sql );

	if($wpdb->get_var("SHOW TABLES LIKE '$contact_field'") == $contact_field) {
		$wpdb->query("INSERT INTO $contact_field
			(field_name , field_active)
			VALUES
			('fullname' , 1),
			('address' , 1),
			('contactnumber' , 0),
			('email' , 1),
			('company' , 0),
			('jobtitle' , 0),
			('imageurl' , 0),
			('websiteurl' , 0),
			('description' , 0)");
	}

	$contact_user = $wpdb->prefix."my_contacts_users";
	$cu_sql = "CREATE TABLE IF NOT EXISTS $contact_user (
	  id int(9) NOT NULL AUTO_INCREMENT,
		fullname varchar(255) DEFAULT '' NOT NULL,
		address varchar(255) DEFAULT '' NOT NULL,
		contactnumber varchar(255) DEFAULT '' NOT NULL,
		email varchar(255) DEFAULT '' NOT NULL,
		company varchar(255) DEFAULT '' NOT NULL,
		jobtitle varchar(255) DEFAULT '' NOT NULL,
		imageurl varchar(255) DEFAULT '' NOT NULL,
		websiteurl varchar(255) DEFAULT '' NOT NULL,
	  description varchar(255) DEFAULT '' NOT NULL,
	  PRIMARY KEY  (id)
	) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $cu_sql );

}
register_activation_hook(__FILE__,'my_contacts_install');

function my_contacts_menu() {
	add_menu_page( 'My Contacts Settings', 'My Contacts', 'manage_options', 'my-contacts-settings', 'my_contacts_settings' );
	add_submenu_page( 'my-contacts-settings', 'My Contacts Users', 'Users', 'manage_options', 'my-contacts-users', 'my_contacts_users');
}
add_action('admin_menu', 'my_contacts_menu');

function my_contacts_settings() {
	include 'mycontacts-settings.php';
}

function my_contacts_users() {
	include 'mycontacts-users.php';
}

function my_contacts_get_fields() {
	global $wpdb;
	$fields_tbl = $wpdb->prefix.'my_contacts_fields';
	$fields = $wpdb->get_results("SELECT * FROM `$fields_tbl`");
	// echo '<pre>',print_r($fields),'</pre>';
	return $fields;
}

?>
